import 'package:flutter/material.dart';
import 'package:login2/UI/CustomInputFiels.dart';
import 'package:login2/main.dart';

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false, title: 'Login App', home: SignUp()));
}

class SignUp extends StatefulWidget {
  @override
  Signupstate createState() {
    return Signupstate();
  }
}

class Signupstate extends State<SignUp> {
  Usrsign usr = new Usrsign();

  final _formsign = GlobalKey<FormState>();
  RegExp idreg = new RegExp(r"([0-9]){9}?((v|V){1})");
  RegExp regmail = RegExp(r"(@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$)");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(200.0)),
              child: Container(
                color: Colors.yellow[700],
                height: 80.0,
                width: 130.0,
              ),
            ),
            Positioned(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(200.0)),
                    child: Container(
                      color: Colors.green,
                      height: 130.0,
                      width: 60.0,
                    ),
                  ),
                ],
              ),
            ),
            Center(
                child: ListView(
              children: <Widget>[
                Container(
                  width: 350,
                  height: 450,
                  child: Form(
                    key: _formsign,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              hintText: 'Full Name',
                              fillColor: Colors.grey[100],
                              filled: true,
                              prefixIcon: Icon(Icons.person),
                            ),
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                            validator: (value) {
                              if (value.isEmpty)
                                return "This can not be empty";
                              else if (value.length > 30)
                                return "Name can not contain more than 30 characters";
                              else
                                return null;
                            },
                            onSaved: (value) {
                              usr.fullname = value;
                              print("Full_Name:" + usr.fullname);
                            },
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              hintText: 'Email',
                              fillColor: Colors.grey[100],
                              filled: true,
                              prefixIcon: Icon(Icons.email),
                            ),
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                            validator: (value) {
                              if (value.isEmpty)
                                return "This can not be empty";
                              else if (!regmail.hasMatch(value))
                                return "Enter valid Email address";
                              else
                                return null;
                            },
                            onSaved: (value) {
                              usr.email = value;
                              print("Email" + usr.email);
                            },
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              hintText: 'Id-Number',
                              fillColor: Colors.grey[100],
                              filled: true,
                              prefixIcon: Icon(Icons.person_outline),
                            ),
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                            validator: (value) {
                              if (value.isEmpty)
                                return "This can not be empty";
                              else if (!idreg.hasMatch(value))
                                return "Enter valid Id number";
                              else
                                return null;
                            },
                            onSaved: (value) {
                              usr.id = value;
                              print("Id:" + usr.id);
                            },
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              hintText: 'Address',
                              fillColor: Colors.grey[100],
                              filled: true,
                              prefixIcon: Icon(Icons.home),
                            ),
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                            validator: (value) {
                              if (value.isEmpty)
                                return "This can not be empty";
                              else if (value.length < 8 && value.length < 200)
                                return "Enter valid address";
                              else
                                return null;
                            },
                            onSaved: (value) {
                              usr.address = value;
                              print("Address:" + usr.address);
                            },
                          ),
                          
                          Container(
                            width: 300,
                            height: 60,
                            child: RaisedButton(
                              onPressed: () {
                                _formsign.currentState.validate();
                                _formsign.currentState.save();
                                // Navigator.push(
                                //     context,
                                //     MaterialPageRoute(
                                //         builder: (context) => HomeScreen()));
                              },
                              color: Colors.blue,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30.0))),
                              child: Text(
                                'Register',
                                style: TextStyle(fontSize: 20.0),
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.topLeft,
                            child: Container(
                              width: 100,
                              height: 30,
                              child: RaisedButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomeScreen()));
                                },
                                color: Colors.green,
                                elevation: 0.2,
                                textColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(30.0))),
                                child: Text(
                                  'Log In',
                                  style: TextStyle(
                                      fontSize: 12.0, color: Colors.black),
                                ),
                              ),
                            ),
                          ),
                        ]),
                  ),
                ),
              ],
              padding: EdgeInsets.fromLTRB(25, 120, 25, 0),
            )),
          ],
        ),
      ),
    );
  }
}

Widget test() {
  return Form(
    child: ListView(
      children: <Widget>[
        TextFormField(
          decoration: InputDecoration(hintText: 'test'),
        )
      ],
    ),
  );
}

class Usrsign {
  String fullname;
  String email;
  String id;
  String address;
  String rpwd;
}
