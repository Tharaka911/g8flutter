import 'package:flutter/material.dart';
import 'package:login2/UI/Farmer.dart';
import 'package:login2/main.dart';


class HomePage extends StatelessWidget{
  static get tag => null;

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(200.0),
                ),
              child: Container(
                color: Colors.yellow[700],
                height: 70.0,
                width: 130.0,
              ),
            ), 
            Positioned(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
             borderRadius: BorderRadius.only(bottomRight: Radius.circular(200.0)),
              child: Container(
                color: Colors.green,
                height: 130.0,
                width: 60.0,
              ),
            ), 
                ],
              ),
            ),
            
            Center(
            child: Container(
              width: 400,
              height: 300,
              child: Column(
                  mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                      Container(
                        width: 170,
                        height: 50,
                        child: RaisedButton(onPressed: (){
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=> FarmerInfo()));},
                        color: Colors.green,textColor: Colors.white,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                        child: Text('Farmer',style: TextStyle(fontSize: 25.0),),),
                    ),
                      Container(
                        width: 170,
                        height: 50,
                        child: RaisedButton(onPressed: (){},
                        color: Colors.lightGreen,textColor: Colors.white,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                        child: Text('Buyer',style: TextStyle(fontSize: 25.0),),),
                      ),
                      Container(
                        width: 170,
                        height: 50,
                        child: RaisedButton(onPressed: (){},
                        color: Colors.yellow[700],textColor: Colors.white,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                        child: Text('Transportor',style: TextStyle(fontSize: 25.0),),),
                      ),
                       Container(
                        width: 120,
                        height: 30,
                        child: RaisedButton(onPressed: (){
                          Navigator.push(context,
                          MaterialPageRoute(builder: (context)=> HomeScreen()));
                        },
                        color: Colors.deepOrange[700],textColor: Colors.white,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                        child: Text('Cancel',style: TextStyle(fontSize: 20.0),),),
                      ),
                  ],
               ),
             ),
            ),
          ], 
        ),
      ),
    );
  }


}