import 'package:flutter/material.dart';
import 'HomePage.dart';

class FarmerInfo extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: new AppBar(     
        backgroundColor: Colors.lightGreen,
        title: Text('Farmer'),
        actions: <Widget>[
          new IconButton(icon: Icon(Icons.home,color: Colors.white,),onPressed: (){
            Navigator.push(context,
            MaterialPageRoute(builder: (context)=> HomePage()));
          },),
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            //header//
            new UserAccountsDrawerHeader(
                 accountName: Text('isuru'),
                 accountEmail: Text('isuru12345@gmail.com'),
            currentAccountPicture: GestureDetector(
              child: new CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(Icons.person,color: Colors.white,),
              ),
            ),
            decoration: new BoxDecoration(
              color: Colors.lightGreen
            ),
            ),
            //body//
          InkWell(
              onTap: (){},
              child: ListTile(
              title: Text('My profile'),
              leading: Icon(Icons.person),
            ),
          ),
          InkWell(
              onTap: (){},
              child: ListTile(
              title: Text('Notification'),
              leading: Icon(Icons.notifications),
            ),
          ),
          InkWell(
              onTap: (){},
              child: ListTile(
              title: Text('Statistics'),
              leading: Icon(Icons.table_chart),
            ),
          ),
          InkWell(
              onTap: (){},
              child: ListTile(
              title: Text('Confirmed order'),
              leading: Icon(Icons.shopping_basket),
            ),
          ),
          InkWell(
              onTap: (){},
              child: ListTile(
              title: Text('Login Out'),
              leading: Icon(Icons.lock_open),
            ),
          ),
          ],
        ),
      ),
    );
  }
}