import 'package:flutter/material.dart';
import '../main.dart';

class CustomInputField extends StatelessWidget{

  Icon fieldIcon;
  String hintText;
  TextFormField txt;

  CustomInputField(this.fieldIcon,this.hintText,this.txt);

  @override
  Widget build(BuildContext context) {
    
    return Container(
                  width: 300,
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    color: Colors.lightGreen,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8.0, 2.0, 2.0, 2.0),
                          child: fieldIcon,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(topRight: Radius.circular(10.0),bottomRight: Radius.circular(10.0))
                          ),
                          width: 250,
                          height: 40,
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: txt,
                            // child: TextField(
                            //  // controller: mycontroler,
                            //   decoration: InputDecoration(
                            //     border:InputBorder.none,
                            //     hintText: hintText,
                            //       fillColor: Colors.white,
                            //       filled: true,
                            //     ),
                            //   style: TextStyle(
                            //      fontSize: 20.0,
                            //      color: Colors.black,
                            //   ),
                            // ),
                          ),
                        ),
                      ],
                    ),
                     
                  ),
                );
  }
}

final mycontroler = TextEditingController();

  void dispose()
  {
    mycontroler.dispose();
    
  }


