import 'package:flutter/material.dart';
import 'package:login2/UI/CustomInputFiels.dart';
import 'package:login2/UI/HomePage.dart';
import 'package:login2/UI/SignUp.dart';

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login App',
      home: HomeScreen()));
}

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenstate createState() {
    return HomeScreenstate();
  }
}

class HomeScreenstate extends State<HomeScreen> {
  Usr _log1 = Usr();

  final _formkey =GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.transparent,
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(200.0)),
              child: Container(
                color: Colors.yellow[700],
                height: 80.0,
                width: 130.0,
              ),
            ),
            Positioned(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius:
                        BorderRadius.only(bottomRight: Radius.circular(200.0)),
                    child: Container(
                      color: Colors.green,
                      height: 130.0,
                      width: 60.0,
                    ),
                  ),
                ],
              ),
            ),
            Center(
              child: Container(
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                width: 380,
                 height: 400,
                child: Form(
                  key: _formkey,
                  autovalidate: false,
                  child: Column(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Material(
                            elevation: 10.0,
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.0)),
                            child: Image.asset(
                              'images/logo.png',
                              width: 80,
                              height: 80,
                            )),
                        TextFormField(
                          
                            decoration: InputDecoration(
                              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0)),),
                              
                              hintText: 'User Name',
                              fillColor: Colors.grey[100],
                              filled: true,
                              prefixIcon: Icon(Icons.person),
                            ),
                            
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                            validator: (uname){
                              if(uname.isEmpty)
                                return "This can not be empty";
                              else
                                return null;
                            },
                            onSaved: (text) {
                              _log1.username = text;
                            },
                          ),
                        TextFormField(
                            decoration: InputDecoration(
                              //border: InputBorder.none,
                              border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0)),),
                              hintText: 'Password',
                              hintStyle: TextStyle(fontSize: 12.0),
                              fillColor: Colors.grey[100],
                              filled: true,
                              prefixIcon: Icon(Icons.lock)
                            ),
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                            validator: (value){
                              if(value.isEmpty)
                                return 'This can not be empty';
                                
                              else if(value.length<8)
                                return "Enter more than 8 character";

                              else
                                return null;
                            },

                            onSaved: (text) {
                              _log1.pwd = text;
                            },
                            obscureText: true,
                          ),
                        Container(
                          width: 380,
                          height: 45,
                          child: RaisedButton(
                            onPressed: () {
                              if(_formkey.currentState.validate())
                              {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomePage()));

                                    _formkey.currentState.save();  
                              }
                              
                              
                              print(_log1.username);
                              print(_log1.pwd);
                            },
                            color: Colors.green,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30.0))),
                            child: Text(
                              'Login',
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ),
                        ),
                        Container(
                          width: 380,
                          height: 45,
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignUp()));
                            },
                            color: Colors.deepOrange,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30.0))),
                            child: Text(
                              'Sign Up',
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ),
                        ),
                      ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Usr {
  String username;
  String pwd;
}

class Createtxt extends TextField {
  String inputtext;
  bool secure;

  Createtxt(this.inputtext, this.secure);

  @override
  // TODO: implement decoration
  InputDecoration get decoration => InputDecoration(
      border: InputBorder.none, fillColor: Colors.white, filled: true);

  @override
  // TODO: implement style
  TextStyle get style => TextStyle(fontSize: 20.0, color: Colors.black);

  @override
  // TODO: implement onChanged
  get onChanged => (text) {
        inputtext = text;
      };
}
